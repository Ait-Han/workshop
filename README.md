# quickstart

### 1. clone the project
```
git clone https://gitlab.com/alpha-caeli/tooling/workshop workshop-<name>
cd workshop-<name>
```

### 2. create certificates for grafana
```
cd grafana
openssl req -x509 -nodes -newkey rsa:2048 -keyout tls.key -out tls.crt -days 365
```

### 3. edit the port mapping for grafana
```
  grafana:
    build: ./grafana
    ports:
      - <changeme>:3000
```


### 4. start the services
```
compose up --build grafana influxdb
```

### 5. edit the python worker

### 6. start it 
```
compose up --build worker
```