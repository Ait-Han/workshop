import openpyxl
from influxdb import InfluxDBClient
import time 


wrkbk = openpyxl.load_workbook("workshop-dev.xlsx")
sh = wrkbk.active

client = InfluxDBClient('_____', 8086, database=____)

print("starting in 2s !")
time.sleep(2)
for i in range(1, sh.max_row+1):
    ip = sh.cell(row=i, column=1).value
    alias = sh.cell(row=i, column=2).value
    cpu = sh.cell(row=i, column=3).value
    disk = sh.cell(row=i, column=4).value
    
    vals = {
        "measurement": "workshop-dev",
        "tags": {"ip": ip, "alias": alias},
        "fields": {
            "cpu": int(cpu),
            "disk": int(disk),
        }
    }
    client.____________([vals])
